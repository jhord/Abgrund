'use strict';

/**
*
* _.js (Abgrund) v0.0.0.0
* 
* Throw objects into a tree-like chasm.
* 
* Synopsis:
* 
*   (function(abgrund) {
*     _._('cc.carbon.Abgrund', abgrund);
* 
*     function say(msg) {
*       console.log(msg);
*     }
* 
*     abgrund.greeting = 'Hi';
*     abgrund.say      = say;
*     abgrund.hi       = function() {
*       this.say(this.greeting + '!');
*     };
* 
*     _.cc.carbon.Abgrund.World = function(adj) {
*       this._adj = adj;
*     }
* 
*     _.cc.carbon.Abgrund.World.prototype.bye = function() {
*       abgrund.say('Good-bye ' + this._adj + ' world.');
*       return this;
*     }
*   })({ });
* 
*   ...
* 
*   <script>
*     _.cc.carbon.Abgrund.hi();
* 
*     var world = new _.cc.carbon.Abgrund.World('cruel');
*     console.log(world.bye());
*   </script>
* 
*   ...
*
*   // node.js
*   var _ = require('abgrund')._;
*
* Gnosis:
* 
*   var obj = _._(ns, obj);
* 
*   Attaches the given object to the namespace tree at the node
*   given by ns.  Namespaces are '.'-separated atoms.  Nodes may
*   be attached to sub-trees if the specified slot is unused.  It
*   is also possible to attach non-objects to nodes but this closes
*   the possibility for sub-tree attachment beneath that node.
* 
*   The nodes are stored in a globally shared namespace accessible
*   via 'window._'.  They are free to mutate this at will since the
*   objects themselves are merged into the namespace.  It's part of
*   the magic.  It's part of the horror.  The point of the namespace
*   is to allow mutability in a controlled corner of the universe.
* 
*   You have been warned.
* 
* Legalosis:
* 
* This software is information.
* It is subject only to local laws of physics.
*
**/

(function(_) {

  function __(ns, obj) {
    var keys = ns.split(/[.]/);
    if(keys.length <= 0) {
      throw('abgrund(' + ns + '): invalid ns');
    }

    var last = keys.splice(keys.length - 1, 1);
    var leaf = _;
    var path = [];

    keys.forEach(function(e) {
      path.push(e);
      if(typeof(leaf[e]) === 'undefined') {
        leaf = leaf[e] = { };
      }
      else if(typeof(leaf[e]) === 'object' || typeof(leaf[e]) === 'function') {
        leaf = leaf[e];
      }
      else {
        throw('abgrund(' + ns + ' -> ' + path.join('.') + '): non-object leaf found in ns');
      }
    });

    if(typeof(leaf[last]) !== 'undefined') {
      throw('abgrund(' + ns + '): ns has already been usurped');
    }

    leaf[last] = obj;
    return obj;
  }

  if(typeof exports !== 'undefined') {exports._ = _};

   // \o/ Lobe den Abgrund
   __('_', __);
})(
  (function() {
    var _ = typeof window === 'undefined' ? process : window;
    return _._ = _._ || { };
  })()
);
