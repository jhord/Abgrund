'use strict';

(function(abgrund) {
  _._('cc.carbon.Abgrund', abgrund);

  function say(msg) {
    console.log(msg);
  }

  abgrund.greeting = 'Hi';
  abgrund.say      = say;
  abgrund.hi       = function() {
    this.say(this.greeting + '!');
  };

  _.cc.carbon.Abgrund.World = function(adj) {
    this._adj = adj;
  }

  _.cc.carbon.Abgrund.World.prototype.bye = function() {
    abgrund.say('Good-bye ' + this._adj + ' world.');
    return this;
  }
})({ });
